# Appli windows pour les olympiades

Donc, je t'ai fait une petite appli pour gérer ton tournoi.

Tu pourras le faire tourner en récupérant Release.zip ou en allant dans :

olympiades/Olympiades/Olympiades/bin/Release

A toi de voir si tu préfères cloner le fichier, il te faudra git par contre.

Là tu va trouver un Olympiades.exe
Tu renseignes le nombre d'équipes, le nombre d'activités.
Et tu cliques sur "Launch".

Tu auras ensuite, à la racine du dossier, un fichier Tournament.txt, qui te sors les activité et les équipes.

Le bouton Nommage n'est pas encore implémenté, ça pourrait permettre de nommer les équipes et les activités direct.

Par contre, pour l'instant je n'ai fait tourner qu'avec 12 équipes et 6 activités max, fait attention que ça ne parte pas en overflow avec plus, je ne peux pas te l'assurer. J'ai pu voir que ça passait aussi avec des équipes impair, du moins ça ne bug pas, mais je n'ai pas gérer la répartition égale des équipes. Donc il faudra peut être faire évoluer le code si on veut s'assurer, sur un nombre impair d'équipes, que tout le monde joue bien un nombre "égal" de matches.
