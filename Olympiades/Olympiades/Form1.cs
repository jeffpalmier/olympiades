﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olympiades
{
    public partial class Form1 : Form
    {
        private Tournament _tournament;
        private List<int> TeamsArray;
        private List<Activity> _activitiesForCheck;
        private List<RotationMatrix> _rotationMatrix;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _tournament = new Tournament();

            int numberOfTeams = 0;
            int.TryParse(this.teams.Text, out numberOfTeams);

            int numberOfActivities = 0;
            int.TryParse(this.activity.Text, out numberOfActivities);

            if (numberOfTeams == 0 || numberOfActivities == 0 )
                informationsBox.Text = "Vous n'avez pas renseigné tous les champs";
            else
                TournamentSort(numberOfTeams, numberOfActivities);
        }
        
        private void TournamentSort(int numberOfTeams, int numberOfActivities)
        {
            _tournament.Activities = BuildActivities(numberOfTeams, numberOfActivities);
            WriteToTxtFile(_tournament);
        }
        

        private void WriteToTxtFile(Tournament tournament)
        {
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(@"Tournament.txt"))
            {

                file.WriteLine("Olympiades Test");
                file.WriteLine("------------------------------------------------------------");

                foreach (var tournamentActivity in tournament.Activities)
                {
                    file.WriteLine($"Activité {tournamentActivity.ActivityId +1}");
                    file.WriteLine("------------------------------------------------------------");

                    foreach (var match in tournamentActivity.Matches)
                    {
                        file.WriteLine($"Tour {match.RotationId +1} | {match.TeamsForMatch[0]} VS {match.TeamsForMatch[1]}");
                    }
                    file.WriteLine("\r\n");
                    file.WriteLine("\r\n");
                }
                
            }
        }
        
        private List<Activity> BuildActivities(int numberOfTeams, int numberOfActivities)
        {
            bool RestartLoop = true;
            List<Activity> activities = null;
            while (RestartLoop)
            {
                RestartLoop = false;
                activities = new List<Activity>();
                _activitiesForCheck = new List<Activity>();
                _rotationMatrix = null;
                TeamsArray = null;
                for (int j = 0; j < numberOfActivities; j++)
                {
                    var activity = new Activity();
                    activity.ActivityId = j;
                    var currentActivity = BuildMatches(j, numberOfTeams);
                    if (currentActivity == null)
                    {
                        RestartLoop = true;
                        break;
                    }
                    activities.Add(currentActivity);
                    _activitiesForCheck.Add(currentActivity);
                }
            }
            return activities;
        }

        private Activity BuildMatches(int activityId, int numberOfTeams)
        {
            var currentActivity = new Activity();
            currentActivity.ActivityId = activityId;
            var nbOfMatchToPlay = numberOfTeams / 2;
            bool CombinaisonKo = true;
            bool ForceRetry = false;
            bool ForceReboot = false;
            int nbOfTry = 0;
            InitTeamsArray(numberOfTeams);
            InitRotationMatrix();
            while (CombinaisonKo)
            {
                ForceRetry = false;
                currentActivity.Matches = new List<Match>();

                InitTeamsArray(numberOfTeams);
                for (int k = 0; k < nbOfMatchToPlay; k++)
                {
                    var currentMatch = RandTeamsByActivity(k);
                    if (currentMatch != null)
                        currentActivity.Matches.Add(currentMatch);
                    else
                        ForceRetry = true;
                }

                CombinaisonKo = ForceRetry || IsThisCombinaisonOk(currentActivity);
                if (nbOfTry >= 20)
                {
                    CombinaisonKo = false;
                    ForceReboot = true;
                }
                else
                    nbOfTry++;
            }

            return ForceReboot ? null : currentActivity;
        }

        private void InitRotationMatrix()
        {
            _rotationMatrix = new List<RotationMatrix>();
            if (!_activitiesForCheck.Any())
            {
                _rotationMatrix = null;
                return;
            }
            foreach (var team in TeamsArray)
            {
                List<int> existingSlots = new List<int>();
                foreach (var check in _activitiesForCheck)
                {
                    foreach (var match in check.Matches)
                    {
                        if(match.TeamsForMatch.Contains(team))
                            existingSlots.Add(match.RotationId);
                    }
                }
                _rotationMatrix.Add(new RotationMatrix {TeamId = team, TimeSlots = existingSlots });
                //_rotationMatrix.Add(new RotationMatrix {TeamId = team, TimeSlots = _activitiesForCheck.SelectMany(x => x.Matches.Where(m => m.TeamsForMatch.Contains(team))).ToList().Select(x => x.RotationId).ToList()});
            }
        }

        private bool IsThisCombinaisonOk(Activity activity)
        {
            foreach (var game in activity.Matches)
            {
                if (CheckExistingConfrontation(game))
                    return true;
                if (CheckAlreadyPlayedThisActivity(game, activity.ActivityId))
                    return true;
                if (CheckTeamPlayOnSameSlot(game))
                    return true;
            }
            return false;
        }

        private bool CheckTeamPlayOnSameSlot(Match game)
        {
            return _activitiesForCheck.SelectMany(x => x.Matches)
                    .Where(g => g.RotationId == game.RotationId)
                        .Any(m => m.TeamsForMatch.Contains(game.TeamsForMatch[0])
                            && m.TeamsForMatch.Contains(game.TeamsForMatch[1]));
        }

        private bool CheckExistingConfrontation(Match game)
        {
            return _activitiesForCheck.Any(a => a.Matches.Any
                    (m => m.TeamsForMatch.Contains(game.TeamsForMatch[0]) 
                     && m.TeamsForMatch.Contains(game.TeamsForMatch[1])));
        }


        private bool CheckAlreadyPlayedThisActivity(Match game, int activityId)
        {
            return _activitiesForCheck.Any(a =>
                    a.ActivityId == activityId && a.Matches.Any
                        (m => m.TeamsForMatch.Contains(game.TeamsForMatch[0]) 
                         && m.TeamsForMatch.Contains(game.TeamsForMatch[1])));
        }

        private void InitTeamsArray(int numberOfTeams)
        {
            TeamsArray = new List<int>();
            for (int i = 0; i < numberOfTeams; i++)
            {
                TeamsArray.Add(i+1);
            }
        }
        
        private Match RandTeamsByActivity(int k)
        {
            Random r = new Random();
            Match match = new Match();
            match.RotationId = k;
            var teamsForThisSlot = CloneTeamsForThisSlot(TeamsArray);
            if (_rotationMatrix != null)
            {
                var teamsToRemove = _rotationMatrix.Where(t => t.TimeSlots.Contains(k)).Select(x => x.TeamId).ToList();
                foreach (var i in teamsToRemove)
                {
                    teamsForThisSlot.Remove(i);
                }
            }

            if (teamsForThisSlot.Count >= 2)
            {
                match.TeamsForMatch[0] = teamsForThisSlot[r.Next(0, teamsForThisSlot.Count)];
                TeamsArray.Remove(match.TeamsForMatch[0]);
                teamsForThisSlot.Remove(match.TeamsForMatch[0]);
                match.TeamsForMatch[1] = teamsForThisSlot[r.Next(0, teamsForThisSlot.Count)];
                TeamsArray.Remove(match.TeamsForMatch[1]);
                return match;
            }

            return null;
        }

        private List<int> CloneTeamsForThisSlot(List<int> teamsArray)
        {
            List<int> clonedItems = new List<int>();
            foreach (var team in teamsArray)
            {
                clonedItems.Add(team);
            }
            return clonedItems;
        }

        private void RemoveTeamsForThisSlot(int k)
        {
            var teamsToRemove = _rotationMatrix.Where(t => t.TimeSlots.Contains(k)).Select(x => x.TeamId).ToList();
            foreach (var i in teamsToRemove)
            {
                TeamsArray.Remove(i);
            }
        }


        private void buttonNommage_Click(object sender, EventArgs e)
        {

            int numberOfTeams = 0;
            int.TryParse(this.teams.Text, out numberOfTeams);

            int numberOfActivities = 0;
            int.TryParse(this.activity.Text, out numberOfActivities);

            if (numberOfTeams == 0 || numberOfActivities == 0)
                informationsBox.Text = "Vous n'avez pas renseigné tous les champs";
            else
                AddNommage(numberOfTeams, numberOfActivities);
            
        }

        private void AddNommage(int numberOfTeams, int numberOfActivities)
        {
            //Label label = new Label();
            //label.Text = "Equipes";
            //label.Location = new Point(10, 10);
            //panel1.Controls.Add(label);
            //for (int i = 0; i < numberOfTeams; i++)
            //{
            //    label = new Label();
            //    label.Text = $"Equipes{i}";
            //}
        }
    }

    internal class RotationMatrix
    {
        public List<int> TimeSlots { get; set; }
        public int TeamId { get; set; }
    }

    internal class Tournament
    {
        public List<Activity> Activities { get; set; }

        public Tournament()
        {
            Activities = new List<Activity>();
        }
    }

    //internal class Rotation 
    //{
    //    public List<Activity> Activities { get; set; }
    //    public int RotationId { get; set; }

    //    public Rotation()
    //    {
    //        Activities = new List<Activity>();
    //    }
    //}

    internal class Activity 
    {
        public int ActivityId { get; set; }
        public List<Match> Matches { get; set; }

        public Activity()
        {
            Matches = new List<Match>();
        }
    }

    internal class Match
    {
        public int[] TeamsForMatch;
        public int RotationId { get; set; }

        public Match()
        {
            TeamsForMatch = new int[2];
        }
    }

        //public int Team1Id { get; set; }
        //public int Team2Id { get; set; }
    
}
